#include "core.h"

static uint32_t _aclkFreq, _smclkFreq, _mclkFreq;

void core_set_clock(uint32_t dcoFreq, uint16_t clkSel, uint16_t clkDiv) {
  // stop watchdog
  WDTCTL = WDTPW | WDTHOLD;
  
  if(dcoFreq >= FRAM_WAIT_STATE_LIMIT) {
    // configure FRAM wait state above 16 MHz
    FRCTL0 = FRCTLPW | NWAITS_1;
  }
  
  // TODO smarter way to configure DCO frequency
  uint16_t clkCfg = DCOFSEL_6;
  if(dcoFreq == 16000000) {
    clkCfg = DCORSEL | DCOFSEL_4;
  }
  
  CSCTL0_H = CSKEY_H;
  CSCTL1 = clkCfg;
  CSCTL2 = clkSel;
  CSCTL3 = clkDiv;
  CSCTL6 = MODCLKREQEN;
  CSCTL0_H = 0;
  
  // cache frequencies
  _aclkFreq = dcoFreq / (1 << ((clkDiv & 0x0700) >> 8));
  _smclkFreq = dcoFreq / (1 << ((clkDiv & 0x0070) >> 4));
  _mclkFreq = dcoFreq / (1 << (clkDiv & 0x0007));
}

uint32_t core_get_aclk_freq() {
  return(_aclkFreq);
}

uint32_t core_get_smclk_freq() {
  return(_smclkFreq);
}

uint32_t core_get_mclk_freq() {
  return(_mclkFreq);
}
