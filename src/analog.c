#include "analog.h"

static analog_cb_t _autoscanCb = NULL;

void analog_init() {
  // wait until reference generator is ready
  while(REFCTL0 & REFGENBUSY);
  
  // enable internal 1.2V reference
  REFCTL0 |= (REFVSEL_0 | REFON);
  
  // disable conversion
  ADC12CTL0 &= ~ADC12ENC;
  
  // ADC12 module on, sample and hold time at 256 ADC12CLK cycles for all channels
  ADC12CTL0 = (ADC12ON | ADC12SHT1_8 | ADC12SHT0_8);
  
  // use sampling timer as sampling signal, ADCCLK = MODOSC
  ADC12CTL1 = ADC12SHS_0 | ADC12SHP | ADC12SSEL_0;
  
  // 12-bit conversion results
  ADC12CTL2 = ADC12RES_2;
  
  // wait for reference generator to settle
  while(!(REFCTL0 & REFGENRDY));
}

void analog_enable_temp() {
  // enable internal temperature sensor
  ADC12CTL3 = ADC12TCMAP;
}

void analog_enable_autoscan(uint8_t* channels, uint8_t numChannels, analog_cb_t cb) {
  if(numChannels > 8) {
    // arbitrary limit on the number of autoscan channels
    return;
  }
  
  // set callback
  if(cb == NULL) {
    return;
  }
  _autoscanCb = cb;
  
  // disable conversion
  ADC12CTL0 &= ~ADC12ENC;
  
  // sequence-of-channels mode
  ADC12CTL1 &= ~ADC12CONSEQ_3;
  ADC12CTL1 |= ADC12CONSEQ_1;
  
  // enable multiple sample and conversion
  ADC12CTL0 |= ADC12MSC;
  
  // set conversion start address to 0 (first memory in autoscan range)
  ADC12CTL3 &= ~0x1F;
  
  // set voltage references and input channels
  for(uint8_t i = 0; i < numChannels; i++) {
    TINY_REGW(ADC12MCTLx(i)) = ADC12VRSEL_1 | (channels[i] & 0x1F);
  }
  
  // set the rest of memories in range to ADC channel 0
  for(uint8_t i = numChannels; i < 8; i++) {
    TINY_REGW(ADC12MCTLx(i)) = ADC12VRSEL_1;
  }
  
  // set EOS bit for the last memory in autoscan range
  ADC12MCTL7 |= ADC12EOS;
  
  // enable interrupt
  ADC12IER0 |= ADC12IE7;
  
  // enable conversion
  ADC12CTL0 |= ADC12ENC;
}

void analog_start_conversion() {
  // enable conversion and start by software trigger
  ADC12CTL0 |= (ADC12ENC | ADC12SC);
}

uint16_t analog_read_blocking(uint8_t chan) {
  // disable conversion
  ADC12CTL0 &= ~ADC12ENC;
  
  // disable multiple sample and conversion
  ADC12CTL0 &= ~ADC12MSC;
  
  // single conversion mode
  ADC12CTL1 &= ~ADC12CONSEQ_3;
  
  // set conversion start address
  ADC12CTL3 &= ~0x1F;
  ADC12CTL3 |= chan;
  
  // disable interrupts
  ADC12IER0 = 0;

  // set voltage reference and input channel
  TINY_REGW(ADC12MCTLx(chan)) = ADC12VRSEL_1 | (chan & 0x1F);
  
  // get address of the interrupt flag register
  TINY_WORD_TYPE* ifgReg = TINY_ADRW(ADC12IFGR0);
  uint16_t ifgFlag = 1 << chan;
  if(chan >= 16) {
    ifgReg = TINY_ADRW(ADC12IFGR1);
    ifgFlag = (1 << (chan - 16));
  }
  
  // software trigger
  analog_start_conversion();
  
  // wait for conversion end
  while(!(TINY_REGW(ifgReg) & ifgFlag));

  // read results, IFG is cleared
  return(TINY_REGW(ADC12MEMx(chan)));
}

// ADC12 interrupt service routine
void __attribute__ ((interrupt(ADC12_VECTOR))) ADC12ISR (void) {
  switch(__even_in_range(ADC12IV, ADC12IV_ADC12RDYIFG)) {
    case ADC12IV_NONE:        break;        // Vector  0:  No interrupt
    case ADC12IV_ADC12OVIFG:  break;        // Vector  2:  ADC12MEMx Overflow
    case ADC12IV_ADC12TOVIFG: break;        // Vector  4:  Conversion time overflow
    case ADC12IV_ADC12HIIFG:  break;        // Vector  6:  ADC12BHI
    case ADC12IV_ADC12LOIFG:  break;        // Vector  8:  ADC12BLO
    case ADC12IV_ADC12INIFG:  break;        // Vector 10:  ADC12BIN
    case ADC12IV_ADC12IFG0:   break;        // Vector 12:  ADC12MEM0
    case ADC12IV_ADC12IFG1:   break;        // Vector 14:  ADC12MEM1
    case ADC12IV_ADC12IFG2:   break;        // Vector 16:  ADC12MEM2
    case ADC12IV_ADC12IFG3:   break;        // Vector 18:  ADC12MEM3
    case ADC12IV_ADC12IFG4:   break;        // Vector 20:  ADC12MEM4
    case ADC12IV_ADC12IFG5:   break;        // Vector 22:  ADC12MEM5
    case ADC12IV_ADC12IFG6:   break;        // Vector 24:  ADC12MEM6
    case ADC12IV_ADC12IFG7: {         // Vector 26:  ADC12MEM7
      // memories 0 - 7 are used for autoscan
      ADC12IFGR0 &= ~BIT7;
      if(_autoscanCb != NULL) {
        _autoscanCb();
      }
    } break;
    case ADC12IV_ADC12IFG8:   break;        // Vector 28:  ADC12MEM8
    case ADC12IV_ADC12IFG9:   break;        // Vector 30:  ADC12MEM9
    case ADC12IV_ADC12IFG10:  break;        // Vector 32:  ADC12MEM10
    case ADC12IV_ADC12IFG11:  break;        // Vector 34:  ADC12MEM11
    case ADC12IV_ADC12IFG12:  break;        // Vector 36:  ADC12MEM12
    case ADC12IV_ADC12IFG13:  break;        // Vector 38:  ADC12MEM13
    case ADC12IV_ADC12IFG14:  break;        // Vector 40:  ADC12MEM14
    case ADC12IV_ADC12IFG15:  break;        // Vector 42:  ADC12MEM15
    case ADC12IV_ADC12IFG16:  break;        // Vector 44:  ADC12MEM16
    case ADC12IV_ADC12IFG17:  break;        // Vector 46:  ADC12MEM17
    case ADC12IV_ADC12IFG18:  break;        // Vector 48:  ADC12MEM18
    case ADC12IV_ADC12IFG19:  break;        // Vector 50:  ADC12MEM19
    case ADC12IV_ADC12IFG20:  break;        // Vector 52:  ADC12MEM20
    case ADC12IV_ADC12IFG21:  break;        // Vector 54:  ADC12MEM21
    case ADC12IV_ADC12IFG22:  break;        // Vector 56:  ADC12MEM22
    case ADC12IV_ADC12IFG23:  break;        // Vector 58:  ADC12MEM23
    case ADC12IV_ADC12IFG24:  break;        // Vector 60:  ADC12MEM24
    case ADC12IV_ADC12IFG25:  break;        // Vector 62:  ADC12MEM25
    case ADC12IV_ADC12IFG26:  break;        // Vector 64:  ADC12MEM26
    case ADC12IV_ADC12IFG27:  break;        // Vector 66:  ADC12MEM27
    case ADC12IV_ADC12IFG28:  break;        // Vector 68:  ADC12MEM28
    case ADC12IV_ADC12IFG29:  break;        // Vector 70:  ADC12MEM29
    case ADC12IV_ADC12IFG30:  break;        // Vector 72:  ADC12MEM30
    case ADC12IV_ADC12IFG31:  break;        // Vector 74:  ADC12MEM31
    case ADC12IV_ADC12RDYIFG: break;        // Vector 76:  ADC12RDY
    default: break;
  }
}
