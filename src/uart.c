#include "uart.h"

#include "gpio.h"

void uart_init(struct uart_cfg_t* uart) {
  if(uart->initialized) {
    return;
  }
  
  if(uart->baudRate <= 0) {
    return;
  }

  // look up pins
  struct pin_t rx, tx;
  if(uart->base == UCA0) {
    tx.port = P2;
    tx.bit = 0;
    rx.port = P2;
    rx.bit = 1;
  } else if(uart->base == UCA1) {
    tx.port = P2;
    tx.bit = 5;
    rx.port = P2;
    rx.bit = 6;
  } else {
    // invalid base address
    return;
  }
  
  // set pin functions
  gpio_select(rx, GPIO_FUNC_SECONDARY);
  gpio_select(tx, GPIO_FUNC_SECONDARY);

  // put peripheral to reset
  TINY_REGW(uart->base + UCxnCTLW0) = UCSWRST;
  
  // use SMCLK as the clock source
  TINY_REGW(uart->base + UCxnCTLW0) |= UCSSEL__SMCLK;
  
  // calculate baud rate config
  float div = (float)core_get_smclk_freq() / uart->baudRate;
  uint16_t divInt = (uint16_t)div;
  uint8_t os = 0;
  uint8_t firstStageMod = 0;
  if(div > 16) {
    // low bit rate, use oversampling mode
    os = 1;
    divInt = ((uint16_t)div) / 16;
    firstStageMod = (div/16.0f - (float)((uint16_t)div/16)) * 16.0f;
  }
  
  // fractional part lookup from a magic datasheet table
  uint16_t divFrac = (float)(div - divInt) * 1000.0f;
  uint8_t secondStageMod = 0xFF;
  if(divFrac < 529) {
    secondStageMod = 0x00;
  } else if(divFrac < 715) {
    secondStageMod = 0x01;
  } else if(divFrac < 835) {
    secondStageMod = 0x02;
  } else if(divFrac < 1001) {
    secondStageMod = 0x04;
  } else if(divFrac < 1252) {
    secondStageMod = 0x08;
  } else if(divFrac < 1430) {
    secondStageMod = 0x10;
  } else if(divFrac < 1670) {
    secondStageMod = 0x20;
  } else if(divFrac < 2147) {
    secondStageMod = 0x11;
  } else if(divFrac < 2224) {
    secondStageMod = 0x21;
  } else if(divFrac < 2503) {
    secondStageMod = 0x22;
  } else if(divFrac < 3000) {
    secondStageMod = 0x44;
  } else if(divFrac < 3335) {
    secondStageMod = 0x25;
  } else if(divFrac < 3575) {
    secondStageMod = 0x49;
  } else if(divFrac < 3753) {
    secondStageMod = 0x4A;
  } else if(divFrac < 4003) {
    secondStageMod = 0x52;
  } else if(divFrac < 4286) {
    secondStageMod = 0x92;
  } else if(divFrac < 4378) {
    secondStageMod = 0x53;
  } else if(divFrac < 5002) {
    secondStageMod = 0x55;
  } else if(divFrac < 5715) {
    secondStageMod = 0xAA;
  } else if(divFrac < 6003) {
    secondStageMod = 0x6B;
  } else if(divFrac < 6254) {
    secondStageMod = 0xAD;
  } else if(divFrac < 6432) {
    secondStageMod = 0xB5;
  } else if(divFrac < 6667) {
    secondStageMod = 0xB6;
  } else if(divFrac < 7001) {
    secondStageMod = 0xD6;
  } else if(divFrac < 7147) {
    secondStageMod = 0xB7;
  } else if(divFrac < 7503) {
    secondStageMod = 0xBB;
  } else if(divFrac < 7861) {
    secondStageMod = 0xDD;
  } else if(divFrac < 8004) {
    secondStageMod = 0xED;
  } else if(divFrac < 8333) {
    secondStageMod = 0xEE;
  } else if(divFrac < 8464) {
    secondStageMod = 0xBF;
  } else if(divFrac < 8572) {
    secondStageMod = 0xDF;
  } else if(divFrac < 8751) {
    secondStageMod = 0xEF;
  } else if(divFrac < 9004) {
    secondStageMod = 0xF7;
  } else if(divFrac < 9170) {
    secondStageMod = 0xFB;
  } else if(divFrac < 9288) {
    secondStageMod = 0xFD;
  } else {
    secondStageMod = 0xFE;
  }
  
  // set baud rate config
  TINY_REGW(uart->base + UCxnBRW)   = divInt;
  TINY_REGW(uart->base + UCxnMCTLW) = ((((uint16_t)secondStageMod) & 0xFF) << 8) | (firstStageMod & 0x0F << 4) | (os & 0x01);
  
  // release from reset
  TINY_REGW(uart->base + UCxnCTLW0) &= ~UCSWRST;
  
  // enable interrupts
  TINY_REGW(uart->base + UCxnIE) = UCRXIE;
  
  // configure DMA
  if(uart->txMode == DMA) {
    // ensure DMA is disabled
    DMA0CTL &= ~DMAEN;
    
    // source byte to destination to byte, increment source, keep destination, single transfer
    DMA0CTL = (DMASBDB | DMASRCINCR_3 | DMADSTINCR_0 | DMADT_0);
    
    // set destination address
    DMA0DA = (uint16_t)&UCA0TXBUF;
  }

  // initialize internal variables
  simple_buffer_init(&(uart->rxRing), uart->rxBuffPtr, uart->rxBuffSize);
  simple_buffer_init(&(uart->txRing), uart->txBuffPtr, uart->txBuffSize);
  uart->initialized = 1;
}

void uart_deinit(struct uart_cfg_t* uart) {
  // configure GPIO
  if(uart->base == UCA0) {
    P2SEL1 &= ~(BIT0 | BIT1);
    P2SEL0 &= ~(BIT0 | BIT1);
  } else if(uart->base == UCA1) {
    P2SEL1 &= ~(BIT5 | BIT6);
    P2SEL0 &= ~(BIT5 | BIT6);
  } else {
    // invalid base address
    return;
  }
  
  // disable interrupts
  TINY_REGW(uart->base + UCxnIE) &= ~UCRXIE;
  uart->initialized = 0;
}

void uart_putchar(struct uart_cfg_t* uart, char ch) {
  if(!uart->initialized) {
    return;
  }

  while(!(TINY_REGW(uart->base + UCxnIFG) & UCTXIFG));
  TINY_REGW(uart->base + UCxnTXBUF) = ch;
  __no_operation();
}

char uart_getchar(struct uart_cfg_t* uart, char *ch) {
  return((char)simple_buffer_take(&(uart->rxRing), (uint8_t*)ch));
}

void uart_write_blocking(struct uart_cfg_t* uart, uint8_t *buff, uint16_t len) {
  if(!uart->initialized) {
    return;
  }
  
  for(uint16_t i = 0; i < len; i++) {
    uart_putchar(uart, buff[i]);
  }
}

void uart_write_interrupt(struct uart_cfg_t* uart, uint8_t *buff, uint16_t len) {
  if(!uart->initialized) {
    return;
  }
  
  // add data to Tx buffer
  for(uint16_t i = 0; i < len; i++) {
    simple_buffer_add(&(uart->txRing), buff[i]);
  }
  
  // enable Tx complete interrupt and send the first byte if we're not sending yet
  if(!(TINY_REGW(uart->base + UCxnIE) & UCTXCPTIE)) {
    TINY_REGW(uart->base + UCxnIE) |= UCTXCPTIE;
    uint8_t ch;
    simple_buffer_take(&(uart->txRing), &ch);
    TINY_REGW(uart->base + UCxnTXBUF) = ch;
  }
}

void uart_write_dma(struct uart_cfg_t* uart, uint8_t *buff, uint16_t len) {
  if(!uart->initialized) {
    return;
  }
  
  // add data to Tx buffer
  for(uint16_t i = 0; i < len; i++) {
    simple_buffer_add(&(uart->txRing), buff[i]);
  }
  
  // check if a transfer has finished (both DMA and UART)
  if(!(DMA0CTL & DMAEN) && (!(TINY_REGW(uart->base + UCxnSTATW) & UCBUSY))) {
    uart_dma_xfer(uart);
  }
}

void uart_dma_xfer(struct uart_cfg_t* uart) {
  // get the number of bytes available in Tx ring buffer
  uint16_t size = simple_buffer_available(&(uart->txRing));
  
  // read the bytes
  uint8_t* data = &(uart->txRing.buff[uart->txRing.readPointer]);
  uart->txRing.readPointer = (uart->txRing.readPointer + size) % uart->txRing.buffSize;
  
  // set source
  DMA0SA = (uint16_t)data;
  
  // enable DMA
  DMA0SZ = size;
  DMA0CTL |= DMAEN;
  
  // start the transfer
  DMACTL0 = DMA0TSEL__DMAREQ;
  DMA0CTL |= DMAREQ;
  
  // set trigger on TX IFG
  DMACTL0 = DMA0TSEL__UCA0TXIFG;
}

void uart_finish_dma(struct uart_cfg_t* uart) {
  if(!uart->initialized) {
    return;
  }
  
  // wait until previous transfer is done (both DMA and UART)
  while((DMA0CTL & DMAEN) || (TINY_REGW(uart->base + UCxnSTATW) & UCBUSY));
  
  // transfer the remaining bytes
  uart_dma_xfer(uart);
  
  // reset read/write pointers (DMA can't handle ring buffer wrap-around)
  uart->txRing.readPointer = 0;
  uart->txRing.writePointer = 0;
}

void uart_write(struct uart_cfg_t* uart, uint8_t *buff, uint16_t len) {
  switch(uart->txMode) {
    case BLOCKING:
      uart_write_blocking(uart, buff, len);
      break;
    case INTERRUPT:
      uart_write_interrupt(uart, buff, len);
      break;
    case DMA:
      uart_write_dma(uart, buff, len);
      break;
  }
}

void uart_poll_input(struct uart_cfg_t* uart, uart_cb_t cb) {
  if(!uart->initialized || !cb) {
    return;
  }
  
  // check if the RX IRQ flag is set
  if(!(TINY_REGW(uart->base + UCxnIFG) & UCRXIFG)) {
    return;
  }
  
  // read the byte, execute callback and add it to Rx buffer
  uint8_t b = (uint8_t)TINY_REGW(uart->base + UCxnRXBUF);
  cb(b);
  simple_buffer_add(&(uart->rxRing), b);
}
