#include "scheduler.h"

typedef struct
{
  volatile int writePointer;
  volatile int readPointer;
  scheduler_fce_t buff[SCHEDULER_EVENT_SIZE];
} scheduler_queue_t;

volatile scheduler_queue_t sInst;

void scheduler_init()
{
  __disable_interrupt();
  sInst.writePointer = 0;
  sInst.readPointer = 0;
  __enable_interrupt();

}

void scheduler_add(scheduler_fce_t fce)
{
  __disable_interrupt();
  if (((sInst.writePointer + 1) % SCHEDULER_EVENT_SIZE) == sInst.readPointer)
  {
    __enable_interrupt();
    return;
  }
  sInst.buff[sInst.writePointer] = fce;
  sInst.writePointer = (sInst.writePointer + 1) % SCHEDULER_EVENT_SIZE;
  __enable_interrupt();
}

void scheduler_take(scheduler_fce_t *fce)
{
  __disable_interrupt();
  if (sInst.readPointer == sInst.writePointer)
  {
    *fce = 0;
    __enable_interrupt();
    return;
  }
  if (fce)
    *fce = sInst.buff[sInst.readPointer];
  sInst.readPointer = (sInst.readPointer + 1) % SCHEDULER_EVENT_SIZE;
  __enable_interrupt();
}
