#include "systick.h"

systick_handler_fce_t callback = 0;

void systick_init(systick_handler_fce_t cb, uint16_t div) {
  callback = cb;
  
  // TACCR0 interrupt enabled
  TA0CCTL0 = CCIE;
  TA0CCR0 = div;
  systick_start();
}

// Timer0_A0 interrupt service routine
void __attribute__ ((interrupt(TIMER0_A0_VECTOR))) Timer0_A0_ISR (void) {
  if(callback) {
    callback();
  }
}

uint32_t systick_uptime(void) {
  return(tl_ticks() / tl_get_tickrate());
}

uint32_t systick_uptime_ms(void) {
  return((tl_ticks() * 1000) / tl_get_tickrate());
}

void systick_start() {
  // SMCLK, UP mode
  TA0CTL = TASSEL__SMCLK | MC__UP;
}

void systick_stop() {
  // SMCLK, UP mode
  TA0CTL = TASSEL__SMCLK | MC__STOP;
}
