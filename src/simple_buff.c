#include "simple_buff.h"

#define enable_interrupt()  _BIS_SR(GIE)
#define disable_interrupt()_BIC_SR(GIE)

void simple_buffer_init(volatile simple_buffer_t* sBuf, unsigned char *buffer, int bufferSize)
{
  if (!sBuf || !buffer || bufferSize < 1)
    return;
  disable_interrupt();
  sBuf->buff = buffer;
  sBuf->buffSize = bufferSize;
  sBuf->writePointer = 0;
  sBuf->readPointer = 0;
  enable_interrupt();
}

int simple_buffer_is_empty(volatile simple_buffer_t* sBuf)
{
  if (!sBuf)
    return 1;
  int retVal;
  disable_interrupt();
  retVal = (sBuf->readPointer == sBuf->writePointer);
  enable_interrupt();
  return retVal;
}

int simple_buffer_is_full(volatile simple_buffer_t* sBuf)
{
  if (!sBuf)
    return 0;
  int retVal;
  disable_interrupt();
  retVal = (((sBuf->writePointer + 1) % sBuf->buffSize) == sBuf->readPointer);
  enable_interrupt();
  return retVal;
}

int simple_buffer_available(volatile simple_buffer_t* sBuf)
{
  if (!sBuf)
    return 0;
  int retVal;
  disable_interrupt();
  retVal = (sBuf->writePointer + sBuf->buffSize - sBuf->readPointer) % sBuf->buffSize;
  enable_interrupt();
  return retVal;
}

int simple_buffer_take(volatile simple_buffer_t* sBuf, unsigned char *out)
{
  if (simple_buffer_is_empty(sBuf))
    return 0;
  disable_interrupt();
  if (out)
    *out = sBuf->buff[sBuf->readPointer];
  sBuf->readPointer = (sBuf->readPointer + 1) % sBuf->buffSize;
  enable_interrupt();
  return 1;
}

int simple_buffer_add(volatile simple_buffer_t* sBuf, unsigned char ch)
{
  int isFull = simple_buffer_is_full(sBuf);
  disable_interrupt();
  if(!isFull)
  {
    sBuf->buff[sBuf->writePointer] = ch;
    sBuf->writePointer = (sBuf->writePointer + 1) % sBuf->buffSize;
  }
  enable_interrupt();
  return isFull;
}
