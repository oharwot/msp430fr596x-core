#include "temp.h"

#include "analog.h"

// from device TLV table memory mapping
#define CALADC12_12V_30C  *((unsigned int *)0x1A1A)   // Temperature Sensor Calibration-30 C
#define CALADC12_12V_85C  *((unsigned int *)0x1A1C)   // Temperature Sensor Calibration-85 C

void temp_init() {
  // enable internal temperature sensor
  analog_enable_temp();
}

int16_t temp_blocking_get() {
  uint16_t raw = analog_read_blocking(ANALOG_CH_TEMP);
  float temp = (float)(((int32_t)raw - CALADC12_12V_30C) * (85 - 30)) / (CALADC12_12V_85C - CALADC12_12V_30C) + 30.0f;
  return(temp*10);
}
