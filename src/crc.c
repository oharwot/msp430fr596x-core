#include "crc.h"

// macro wrapper to calculate CRC over an address range given by two linker symbols
#define CRC_ADDR_RANGE(start, end) { extern char start, end; crc_addr_range(&start, &end); }

static void crc_addr_range(char* start, char* end) {
  char* origin = start;
  unsigned int length = end - origin;
  for(unsigned int i = 0; i < length; i++) {
    CRCDIRB = (*(origin + i));
  }
}

uint16_t crc_rom_calc() {
  // write initial value
  CRCINIRES = 0xFFFF;
  
  // .lower.rodata > ROM
  CRC_ADDR_RANGE(__lower_rodata_start, __lower_rodata_end);
  
  // .rodata > ROM
  CRC_ADDR_RANGE(__rodata_start, __rodata_end);
  
  // .rodata2 > ROM
  CRC_ADDR_RANGE(__rodata2_start, __rodata2_end);
  
  // .lower.text > ROM
  CRC_ADDR_RANGE(__lower_text_start, __lower_text_end);
  
  // .text > ROM
  CRC_ADDR_RANGE(_start, _etext);
  
  // read the result
  return(CRCINIRES);
}
