#include "spi.h"

static struct pin_t _cs;

void spi_init(struct spi_cfg_t cfg) {
  if(cfg.initialized) {
    return;
  }
  
  // look up pins (UCA0 is the default)
  struct pin_t sck =  { .port = P1, .bit = 5 };
  struct pin_t mosi = { .port = P2, .bit = 0 };
  struct pin_t miso = { .port = P2, .bit = 1 };
  if(cfg.periphBase == UCA1) {
    sck.port  = P2;
    sck.bit   = 4;
    mosi.port = P2;
    mosi.bit  = 5;
    miso.port = P2;
    miso.bit  = 6;
  } else if(cfg.periphBase == UCB0) {
    sck.port  = P2;
    sck.bit   = 2;
    mosi.port = P1;
    mosi.bit  = 6;
    miso.port = P1;
    miso.bit  = 7;
  }
  
  // set pin functions
  gpio_select(sck, GPIO_FUNC_SECONDARY);
  gpio_select(mosi, GPIO_FUNC_SECONDARY);
  gpio_select(miso, GPIO_FUNC_SECONDARY);
  
  // set CS
  gpio_set_output(cfg.csPin);
  gpio_set_level(cfg.csPin, GPIO_HIGH);
  _cs = cfg.csPin;
  
  // configure USCI for SPI operation
  TINY_REGW(cfg.periphBase + UCxnCTLW0)  = UCSWRST;                           // put to reset
  TINY_REGW(cfg.periphBase + UCxnCTLW0) |= UCSSEL__SMCLK;                     // use SMCLK as clock source
  TINY_REGW(cfg.periphBase + UCxnCTLW0) |= UCMST | UCSYNC | UCMSB;            // 3-pin, 8-bit SPI master, CKPH=0. CKPL=0
  TINY_REGW(cfg.periphBase + UCxnBRW)    = core_get_smclk_freq() / cfg.freq;  // set SPI clock frequency
  TINY_REGW(cfg.periphBase + UCxnMCTLW)  = 0;                                 // no modulation
  TINY_REGW(cfg.periphBase + UCxnCTLW0) &= ~UCSWRST;                          // release from reset
}

void spi_blocking_transaction(struct spi_cfg_t cfg, uint8_t* tx_data, uint8_t* rx_data, uint8_t len) {
  uint8_t ch;
  
  // empty RX buffer
  while(TINY_REGW(cfg.periphBase + UCxnIFG) & UCRXIFG) {
    ch = (uint8_t)TINY_REGW(cfg.periphBase + UCxnRXBUF);
    ch++;
  }
  
  // CS low
  gpio_set_level(_cs, GPIO_LOW);
  
  // perform transaction
  for(uint8_t i = 0; i < len; i++) {
    // send one byte
    if(tx_data) {
      TINY_REGW(cfg.periphBase + UCxnTXBUF) = tx_data[i];
    } else {
      TINY_REGW(cfg.periphBase + UCxnTXBUF) = 0x00;
    }
    
    // wait for Tx+Rx done interrupt
    while(!(TINY_REGW(cfg.periphBase + UCxnIFG) & UCTXIFG) && !(TINY_REGW(cfg.periphBase + UCxnIFG) & UCRXIFG));
    
    // read one byte
    if(rx_data) {
      rx_data[i] = TINY_REGW(cfg.periphBase + UCxnRXBUF);
    } else {
      ch = TINY_REGW(cfg.periphBase + UCxnRXBUF);
    }
    
    // wait until everything is ready for the next byte
    while(TINY_REGW(cfg.periphBase + UCxnSTATW) & UCBUSY);
  }
  
  // CS high
  gpio_set_level(_cs, GPIO_HIGH);
}
