#ifndef _MSP430FR596X_CRC_H
#define _MSP430FR596X_CRC_H

#include "core.h"

uint16_t crc_rom_calc();

#endif
