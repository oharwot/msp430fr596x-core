#ifndef _MSP430FR596X_CORE_H
#define _MSP430FR596X_CORE_H

#include <msp430.h>
#include <stdint.h>

#include <tinylibc.h>

// memory section access macros
#define FRAM                           __attribute__ ((section (".fram")))
#define PERSIST                        __attribute__ ((section (".persistent")))

// Universal Serial Communication Interface aliases
#define UCA0                           (TINY_ADRW(UCA0CTLW0))
#define UCA1                           (TINY_ADRW(UCA1CTLW0))
#define UCxnCTLW0                      (TINY_ADRW(UCA0CTLW0)  - UCA0)
#define UCxnCTLW1                      (TINY_ADRW(UCA0CTLW1)  - UCA0)
#define UCxnBRW                        (TINY_ADRW(UCA0BRW)    - UCA0)
#define UCxnMCTLW                      (TINY_ADRW(UCA0MCTLW)  - UCA0)
#define UCxnSTATW                      (TINY_ADRW(UCA0STATW)  - UCA0)
#define UCxnRXBUF                      (TINY_ADRW(UCA0RXBUF)  - UCA0)
#define UCxnTXBUF                      (TINY_ADRW(UCA0TXBUF)  - UCA0)
#define UCxnABCTL                      (TINY_ADRW(UCA0ABCTL)  - UCA0)
#define UCxnIRCTL                      (TINY_ADRW(UCA0IRCTL)  - UCA0)
#define UCxnIE                         (TINY_ADRW(UCA0IE)     - UCA0)
#define UCxnIFG                        (TINY_ADRW(UCA0IFG)    - UCA0)
#define UCxnIV                         (TINY_ADRW(UCA0IV)     - UCA0)

#define UCB0                           (TINY_ADRW(UCB0CTLW0))
#define UCBnSTATW                      (TINY_ADRW(UCB0STATW)  - UCB0)
#define UCBnIE                         (TINY_ADRW(UCB0IE)     - UCB0)
#define UCBnIFG                        (TINY_ADRW(UCB0IFG)    - UCB0)
#define UCBnIV                         (TINY_ADRW(UCB0IV)     - UCB0)
#define UCBnI2COA0                     (TINY_ADRW(UCB0I2COA0) - UCB0)
#define UCBnI2CSA                      (TINY_ADRW(UCB0I2CSA)  - UCB0)

// ADC12_B aliases
#define ADC12MCTLx(x)                  (TINY_ADRW(ADC12MCTL0) + (x))
#define ADC12MEMx(x)                   (TINY_ADRW(ADC12MEM0)  + (x))

// configure FRAM wait states above this clock frequency
#define FRAM_WAIT_STATE_LIMIT          (16000000)

void core_set_clock(uint32_t dcoFreq, uint16_t clkSel, uint16_t clkDiv);

uint32_t core_get_aclk_freq();
uint32_t core_get_smclk_freq();
uint32_t core_get_mclk_freq();

#endif
