#ifndef _MSP430FR596X_KISS_H
#define _MSP430FR596X_KISS_H

#include <stdint.h>

#include <uart.h>
#include <scheduler.h>

#define FEND  							(0xC0)
#define FESC  							(0xDB)
#define TFEND 							(0xDC)
#define TFESC 							(0xDD)
#define TNC_DATA						(0x00)

typedef enum {
	KISS_MODE_NOT_STARTED,  //!< No start detected
  KISS_MODE_SKIP_TNC,     //!< Skipping TNC byte
	KISS_MODE_STARTED,      //!< Started on a KISS frame
	KISS_MODE_ESCAPED,      //!< Rx escape character
	//KISS_MODE_SKIP_FRAME,   //!< Skip remaining frame, wait for end character
} kiss_mode_t;

typedef void (*kiss_cb_fce_t)(uint8_t *buff, uint16_t len);

struct kiss_inst_t {
	kiss_mode_t mode;
	kiss_cb_fce_t cb;
	struct uart_cfg_t* uart;
	uint8_t rxBuffPos;
	uint8_t* rxBuffPtr;
	uint16_t rxBuffSize;
};

int kiss_send(struct kiss_inst_t* inst, uint8_t *buff, uint16_t len);
void kiss_check(struct kiss_inst_t* inst, uint8_t b);

#endif // _MSP430FR596X_KISS_H
