#ifndef _MSP430FR596X_ANALOG_H
#define _MSP430FR596X_ANALOG_H

#include "core.h"

#define ANALOG_CH_TEMP          (30)

typedef void (*analog_cb_t)(void);

void analog_init();

void analog_enable_temp();
void analog_enable_autoscan(uint8_t* channels, uint8_t numChannels, analog_cb_t cb);

void analog_start_conversion();
uint16_t analog_read_blocking(uint8_t chan);

#endif
