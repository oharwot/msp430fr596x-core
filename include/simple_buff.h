#ifndef _MSP430FR596X_CIRC_BUFF_H
#define _MSP430FR596X_CIRC_BUFF_H

#include "core.h"

typedef struct
{
  int writePointer;
  int readPointer;
  int buffSize;
  unsigned char *buff;
} simple_buffer_t;

void simple_buffer_init(volatile simple_buffer_t* sBuf, unsigned char* buffer, int bufferSize);
int simple_buffer_is_empty(volatile simple_buffer_t* sBuf);
int simple_buffer_is_full(volatile simple_buffer_t* sBuf);
int simple_buffer_available(volatile simple_buffer_t* sBuf);
int simple_buffer_take(volatile simple_buffer_t* sBuf, unsigned char *out);
int simple_buffer_add(volatile simple_buffer_t* sBuf, unsigned char ch);

#endif
