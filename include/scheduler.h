#ifndef _MSP430FR596X_SCHEDULER_H
#define _MSP430FR596X_SCHEDULER_H

#include "core.h"

#define SCHEDULER_EVENT_SIZE    (10)

typedef void (*scheduler_fce_t)(void);

void scheduler_init(void);
void scheduler_add(scheduler_fce_t fce);
void scheduler_take(scheduler_fce_t *fce);

#endif
