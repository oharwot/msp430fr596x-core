#ifndef _MSP430FR596X_GPIO_H
#define _MSP430FR596X_GPIO_H

#include "core.h"

// pin function
#define GPIO_FUNC_DIGITAL_IO          (0)
#define GPIO_FUNC_PRIMARY             (1)
#define GPIO_FUNC_SECONDARY           (2)
#define GPIO_FUNC_TERTIARY            (3)

// pin levels
#define GPIO_LOW                      (0)
#define GPIO_HIGH                     (1)
#define GPIO_PULLUP                   (GPIO_HIGH)
#define GPIO_PULLDOWN                 (GPIO_LOW)

// pin definition structure
struct pin_t {
  uint8_t* port;
  uint8_t bit;
};

// aliases for linker symbols
#define P1                      (TINY_ADRB(P1IN))
#define P2                      (TINY_ADRB(P2IN))
#define P3                      (TINY_ADRB(P3IN))
#define P4                      (TINY_ADRB(P4IN))
#define PJ                      (TINY_ADRB(PJIN))
#define PxIN                    (TINY_ADRB(P1IN) - P1)
#define PxOUT                   (TINY_ADRB(P1OUT) - P1)
#define PxDIR                   (TINY_ADRB(P1DIR) - P1)
#define PxSEL0                  (TINY_ADRB(P1SEL0) - P1)
#define PxSEL1                  (TINY_ADRB(P1SEL1) - P1)

void gpio_select(struct pin_t pin, uint8_t func);
void gpio_set_output(struct pin_t pin);
void gpio_set_input(struct pin_t pin);
void gpio_set_analog(struct pin_t pin);
void gpio_set_level(struct pin_t pin, uint8_t lvl);
void gpio_toggle(struct pin_t pin);
uint8_t gpio_read_port(struct pin_t pin);
uint8_t gpio_read_pin(struct pin_t pin);

#endif
